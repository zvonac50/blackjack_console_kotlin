var GAMEOVER: Boolean = false
var playerLost: Boolean = false

var poppedCards: MutableList<Card> = mutableListOf()

fun main() {
    while (!GAMEOVER) {
        println("Game is starting, cards are being shuffled!")
        makeUIDelay(2000)
        var deck = CardsDeck()
        val players = mutableListOf<Player>()
        players.add(HumanPlayer("Zvonimir", deck))
        players.add(ComputerPlayer(deck))

        poppedCards.add(players[0].popCard())
        poppedCards.add(players[1].popCard())
        poppedCards.add(players[0].popCard())
        poppedCards.add(players[1].popCard())
        println("\n${players[0].name}'s first two cards on table are: ")
        makeUIDelay(1000)
        println("${poppedCards[0]}")
        makeUIDelay(1000)
        println("${poppedCards[2]}")
        makeUIDelay(1000)
        println("\n${players[1].name} got this card facing up: ${poppedCards[1]}\nand one unknown card facing down\n")

        while (players[0].currentPoints < 21) {
            var userInput: String
            do {
                print("Do You want to HIT or STAND: ")
                userInput = readLine().toString().toLowerCase()
            } while ((userInput != "hit") && (userInput != "stand"))
            if (userInput == "hit") {
                poppedCards.add(players[0].popCard())
                makeUIDelay(1000)
                println("\n${players[0].name} got this card: ${poppedCards[poppedCards.size - 1]}")
            } else {
                break
            }
        }
        println("${players[0].name}'s current points are: ${players[0].currentPoints}\n")

        println("It's dealers turn now! Take Your seat, we are starting off!")
        makeUIDelay(3000)
        println("${players[1].name}'s card facing down was: ${poppedCards[3]}")
        makeUIDelay(1000)
        do {
            poppedCards.add(players[1].popCard())
            if (poppedCards[poppedCards.size - 1] != Card()) {
                println("${players[1].name} hit a card and got this card: ${poppedCards[poppedCards.size - 1]}")
                makeUIDelay(1000)
            }
        } while (poppedCards[poppedCards.size - 1] != Card())
        println("${players[1].name}'s current points are: ${players[1].currentPoints}")

        if (((players[1].currentPoints > players[0].currentPoints) && players[1].currentPoints <= 21) || players[0].currentPoints > 21)
            println("\n${players[1].name} won!!!")
        else
            println("\nPlayer won!!!")

        println("\nDo You wanna play another round? Yes/No(!yes)")
        print("Your answer: ")
        if (readLine()?.toLowerCase() != "yes") {
            GAMEOVER = true
            println("GAME HAS ENDED!")
        } else
            poppedCards.clear()
            println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
    }
}

private fun makeUIDelay(milliseconds: Long) {
    Thread.sleep(milliseconds)
}