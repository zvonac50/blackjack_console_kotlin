data class Card(val color: String, val cardValue: Int, val picture: String) {
    constructor(color: String, cardValue: Int) : this(color, cardValue, "")
    constructor() : this("", 0, "")

    override fun toString(): String {
        return if (this.picture == "")
            "${this.color} card with number ${this.cardValue}"
        else
            "${this.color} card with number ${this.cardValue} and picture of a ${this.picture}"
    }
}